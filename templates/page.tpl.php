<?php
// Sun, Sep, 2 17.11 Home
/**
 * @file
 * Default theme implementation to display a single Drupal page.
 *
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see template_process()
 * @see html.tpl.php
 * @see /modules/system/page.tpl.php
 */
?>
<!--Navbar
==============================-->
    <header class="navbar navbar-inverse navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container">

          <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
                <?php if ($logo): ?>
        <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo" class="brand">
          <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
        </a>
      <?php endif; ?>

          <div class="nav-collapse collapse">
            <p class="navbar-text pull-right">
              <a href="#" class="navbar-link">.</a>
            </p>
            <!--
            <ul class="nav">
              <li class="active"><a href="#">Home</a></li>
              <li><a href="#">About</a></li>
              <li><a href="#">Contact</a></li>
            </ul>
            -->
                <?php if ($main_menu || $secondary_menu): ?>

        <?php print theme('links__system_main_menu', array('links' => $main_menu, 'attributes' => array('id' => 'main-menu', 'class' => array('nav', 'links', 'inline', 'clearfix')), 'heading' => t(''))); ?>
        <?php print theme('links__system_secondary_menu', array('links' => $secondary_menu, 'attributes' => array('id' => 'secondary-menu', 'class' => array('nav')), 'heading' => t(''))); ?>

    <?php endif; ?>
          </div><!--/.nav-collapse -->



          <?php print render($page['header']); ?>
        </div>
      </div>
    </header>
<!--Stage
==============================-->
     <div class="container">
      <div class="row">
        <div class="span12">
      <?php if ($page['stage']): ?><div id="highlighted"><?php print render($page['stage']); ?></div><?php endif; ?>
        </div><!--/span -->
        </div><!--/row -->
        </div><!--/container-->
<!--Content
==============================-->
      <div class="container">
      <div class="row">


    <?php if ($breadcrumb): ?>
      <div id="breadcrumb"><?php print $breadcrumb; ?></div>
    <?php endif; ?>

    <?php print $messages; ?>



            <?php if ($page['sidebar_first']): ?>
 <aside class="span3">
   <div class="well sidebar-nav">
          <?php print render($page['sidebar_first']); ?>
 </div></aside>
      <?php endif; ?>




 <section class="<?php print $spanX; ?>">

          <div class="hero-unit">
            <h1>Hello, world!</h1>
            <p>This is a template for a simple marketing or informational website. It includes a large callout called the hero unit and three supporting pieces of content. Use it as a starting point to create something more unique.</p>
            <p><a class="btn btn-primary btn-large">Learn more »</a></p>
          </div>


        <a id="main-content"></a>
        <?php print render($title_prefix); ?>
        <?php if ($title): ?><h1 class="title" id="page-title"><?php print $title; ?></h1><?php endif; ?>
        <?php print render($title_suffix); ?>
        <?php if ($tabs): ?><div class="tabs"><?php print render($tabs); ?></div><?php endif; ?>
         <?php if ($page['help']): ?>
          <div class="well"><?php print render($page['help']); ?></div>
        <?php endif; ?>
            <?php if ($action_links): ?><ul class="action-links"><?php print render($action_links); ?></ul><?php endif; ?>
        <?php  print render($page['content']); ?>
        <?php print $feed_icons; ?>


 </section><!--/span6 /span9 or /span12-->







      <?php if ($page['sidebar_second']): ?>
 <aside class="span3">
             <div class="well sidebar-nav">
          <?php print render($page['sidebar_second']); ?>
 </div> </aside>
      <?php endif; ?>


          </div><!--/row -->
        </div><!--/container-->

<!--Footer
==============================-->

      <footer>
            <?php print render($page['footer']); ?>
      </footer>