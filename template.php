<?php

/**
 * Implements hook_preprocess_page()
 *
 * @param type $variables
 *
 * We create a new element that keep tracks of scafolding grid columns (width) for content area.
 * This $variables['spanX'] will be available as $spanX in the page
 */
function coriolis_preprocess_page(&$variables) {

  if (!empty($variables['page']['sidebar_first']) && !empty($variables['page']['sidebar_second'])) {
    $variables['spanX'] = 'span6';
  } elseif (!empty($variables['page']['sidebar_first'])) {
    $variables['spanX'] = 'span9';
  } elseif (!empty($variables['page']['sidebar_second'])) {
    $variables['spanX'] = 'span9';
  } else {
    $variables['spanX'] = 'span12';
  }
}

/**
  function coriolis_region(&$variables) {
  //if($variables['region'] == "sidebar_first")
  //$variables['classes_array'][] = 'well';
  }
 */

/**
 * Implements hook_js_alter()
 * Twitter Bootstrap needs an updated version of jquery
 *
 * @param type $javascript
 * An array of all JavaScript being presented on the page.
 */
function coriolis_js_alter(&$javascript) {
  $javascript['misc/jquery.js']['data'] = drupal_get_path('theme', 'coriolis') . '/js/jquery-1.8.0.min.js';
}

/*
 * hook_theme()
 * We need this to implement our custom theme_links, respectively links_system_main_menu
 *
 */
/* function coriolis_theme() {
  return array(
  'coriolis_links' => array(
  'variables' => array('links' => array(), 'attributes' => array(), 'heading' => NULL),
  ),
  );
  }

  function theme_coriolis_links ($variables) {

  }
 */

/**
 * Implement theme_links to override the default main menu html output
 * @param type $variables
 * @return string
 */
function coriolis_links__system_main_menu($variables) {
  $html = '';
  $html .= "  <ul class='nav'>\n";
  foreach ($variables['links'] as $key => $value) {
    $pos = strpos($key, 'active-trail');
    if ($pos > 0) {
      //$options = array();
      //$options['attributes']['class'][] = 'active';
      $html .= "<li class=\"active\">" . l($value['title'], $value['href'], $value) . "</li>";
    } else {
      $html .= "<li>" . l($value['title'], $value['href'], $value) . "</li>";
    }
  }
  $html .= "  </ul>\n";
  return $html;
}

function coriolis_links__system_secondary_menu($variables) {
  $html = '';
  $html .= "  <ul class='nav'>\n";
  foreach ($variables['links'] as $key => $value) {
    $pos = strpos($key, 'active-trail');
    if ($pos > 0) {
      $html .= "<li class=\"active\">" . l($value['title'], $value['href'], $value) . "</li>";
    } else {
      $html .= "<li>" . l($value['title'], $value['href'], $value) . "</li>";
    }
  }
  $html .= "  </ul>\n";
  return $html;
}

